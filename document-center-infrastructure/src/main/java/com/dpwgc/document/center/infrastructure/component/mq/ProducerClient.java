package com.dpwgc.document.center.infrastructure.component.mq;

import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ProducerClient implements InitializingBean {

    @Value("${rocketmq.namesrvAddr}")
    private String namesrvAddr;

    @Value("${rocketmq.producerGroup}")
    private String producerGroup;

    @Value("${rocketmq.eventBusTopic}")
    private String eventBusTopic;

    @Override
    public void afterPropertiesSet() throws Exception {
        /*
        //1.创建消息生产者producer，并制定生产者组名
        DefaultMQProducer producer = new DefaultMQProducer(producerGroup);
        //2.指定Nameserver地址
        producer.setNamesrvAddr(namesrvAddr);
        //3.启动producer
        producer.start();

         */
    }
}
