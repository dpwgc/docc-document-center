package com.dpwgc.document.center.infrastructure.component.mq;

import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.MessageSelector;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;
import org.apache.rocketmq.common.message.MessageExt;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ConsumerClient implements InitializingBean {

    @Value("${rocketmq.namesrvAddr}")
    private String namesrvAddr;

    @Value("${rocketmq.consumerGroup}")
    private String consumerGroup;

    @Value("${rocketmq.eventBusTopic}")
    private String eventBusTopic;

    @Override
    public void afterPropertiesSet() throws Exception {
        /*
        DefaultMQPushConsumer consumer = new DefaultMQPushConsumer(consumerGroup);
        //2.指定Nameserver地址
        consumer.setNamesrvAddr(namesrvAddr);
        //3.订阅主题Topic和Tag
        consumer.subscribe(eventBusTopic, MessageSelector.bySql("i>5"));
        //TODO 使用SQL过滤需要broker开启开启对filter的支持enablePropertyFilter=true

        //4.设置回调函数，处理消息
        consumer.registerMessageListener(new MessageListenerConcurrently() {

            //接受消息内容
            @Override
            public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> msgs, ConsumeConcurrentlyContext context) {
                for (MessageExt msg : msgs) {
                    System.out.println("consumeThread=" + Thread.currentThread().getName() + "," + new String(msg.getBody()));
                }
                return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
            }
        });
        //5.启动消费者consumer
        consumer.start();

         */
    }
}
