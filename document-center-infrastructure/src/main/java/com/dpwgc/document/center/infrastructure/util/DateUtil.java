package com.dpwgc.document.center.infrastructure.util;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 日期工具
 */
public class DateUtil {

    /**
     * 日期格式
     **/
    public static final String DATE_PATTERN_HH_MM = "HH:mm";
    public static final String DATE_PATTERN_HH_MM_SS = "HH:mm:ss";
    public static final String DATE_PATTERN_YYYY_MM_DD = "yyyy-MM-dd";
    public static final String DATE_PATTERN_YYYY = "yyyy";
    public static final String DATE_PATTERN_YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";
    public static final String DATE_PATTERN_YYYY_MM_DD_HH_MM_SS_1 = "yyyy/MM/dd HH:mm:ss";
    public static final String DATE_PATTERN_YYYY_MM_DD_HH_MM_SS_2 = "yyyy-MM-dd HH:mm:ss SSS";
    public static final String DATE_PATTERN_YYYY_MM_DD_HH_MM = "yyyy-MM-dd HH:mm";
    public static final String DATE_PATTERN_MM_DD_HH_MM_CN = "MM月dd日 HH:mm";
    public static final String DATE_PATTERN_YYYY_MM_DD_HH_MM_CN = "yyyy年MM月dd日 HH:mm";
    public static final String DATE_PATTERN_YYYY_MM_DD_1 = "yyyyMMdd";
    public static final String DATE_PATTERN_YYYY_MM_DD_HH_MM_1 = "yyyyMMddHHmm";
    public static final String DATE_PATTERN_YYYY_MM_DD_HH_MM_2 = "yyyyMMddHHmmss";
    public static final Date initTime = string2Date("1970-01-01 00:00:00");

    public static String date2String(Date date) {
        return date2String(date, DATE_PATTERN_YYYY_MM_DD_HH_MM_SS);
    }

    public static String date2String(Date date, String pattern) {
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        return format.format(date);
    }

    public static Date string2Date(String dateString) {
        return string2Date(dateString, DATE_PATTERN_YYYY_MM_DD_HH_MM_SS);
    }

    public static Date string2Date(String dateString, String pattern) {
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        ParsePosition pos = new ParsePosition(0);
        return format.parse(dateString, pos);
    }

    /***
     * 获取当前时间 yyyy-MM-dd
     */
    public static Date getCurrentDate() {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            return sdf.parse(sdf.format(new Date()));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /***
     * 获取当前时间 yyyy-MM-dd HH:mm:ss
     */
    public static Date getCurrentDateTime() {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            return sdf.parse(sdf.format(new Date()));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
