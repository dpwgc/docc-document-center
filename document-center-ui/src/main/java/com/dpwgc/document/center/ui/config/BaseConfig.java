package com.dpwgc.document.center.ui.config;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 基础配置读取
 */
@Component
public class BaseConfig implements InitializingBean {

    @Value("${spring.application.name}")
    private String applicationName;

    @Value("${server.port}")
    private String serverPort;

    @Value("${spring.datasource.url}")
    private String datasourceUrl;

    @Value("${elasticsearch.url}")
    private String elasticsearchUrl;

    @Value("${elasticsearch.indexPrefix}")
    private String indexPrefix;

    @Value("${cluster.datacenterId}")
    private String datacenterId;

    @Value("${cluster.workerId}")
    private String workerId;

    @Value("${server.servlet.context-path}")
    private String contextPath;

    /**
     * spring boot项目启动后自动执行
     */
    @Override
    public void afterPropertiesSet() {
        System.out.println("\n==================== configuration ====================" +
                "\n<application> [name]: "+applicationName +
                "\n<datasource> [url]: "+datasourceUrl +
                "\n<elasticsearch> [url]: "+elasticsearchUrl+"    [index_prefix]: "+indexPrefix +
                "\n<cluster> [datacenter_id]: "+datacenterId+"    [worker_id]: "+workerId +
                "\n<monitor> [url]: http://localhost:"+serverPort+contextPath+"/monitor.html" +
                "\n<swagger api doc> [url]: http://localhost:"+serverPort+contextPath+"/doc.html" +
                "\n<graphiql page> [url]: http://localhost:"+serverPort+contextPath+"/graphiql" +
                "\n==================== configuration ====================\n");
    }
}
